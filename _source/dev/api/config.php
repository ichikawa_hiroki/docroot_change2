<?php
/**
 * config
 */


define('DOC_ROOT', $_SERVER['DOCUMENT_ROOT']);
define('PATH_DATAFILE', dirname(dirname(__FILE__)) . '/data/data.csv');
define('PATH_CURRENTFILE', dirname(dirname(__FILE__)) . '/data/current.txt');

define('DEPTH_DIG_DIRECTORY', 3);
