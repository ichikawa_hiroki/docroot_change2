/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/assets/js/app.es6");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/path-browserify/index.js":
/*!***********************************************!*\
  !*** ./node_modules/path-browserify/index.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("/* WEBPACK VAR INJECTION */(function(process) {// .dirname, .basename, and .extname methods are extracted from Node.js v8.11.1,\n// backported and transplited with Babel, with backwards-compat fixes\n\n// Copyright Joyent, Inc. and other Node contributors.\n//\n// Permission is hereby granted, free of charge, to any person obtaining a\n// copy of this software and associated documentation files (the\n// \"Software\"), to deal in the Software without restriction, including\n// without limitation the rights to use, copy, modify, merge, publish,\n// distribute, sublicense, and/or sell copies of the Software, and to permit\n// persons to whom the Software is furnished to do so, subject to the\n// following conditions:\n//\n// The above copyright notice and this permission notice shall be included\n// in all copies or substantial portions of the Software.\n//\n// THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS\n// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF\n// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN\n// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,\n// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR\n// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE\n// USE OR OTHER DEALINGS IN THE SOFTWARE.\n\n// resolves . and .. elements in a path array with directory names there\n// must be no slashes, empty elements, or device names (c:\\) in the array\n// (so also no leading and trailing slashes - it does not distinguish\n// relative and absolute paths)\nfunction normalizeArray(parts, allowAboveRoot) {\n  // if the path tries to go above the root, `up` ends up > 0\n  var up = 0;\n  for (var i = parts.length - 1; i >= 0; i--) {\n    var last = parts[i];\n    if (last === '.') {\n      parts.splice(i, 1);\n    } else if (last === '..') {\n      parts.splice(i, 1);\n      up++;\n    } else if (up) {\n      parts.splice(i, 1);\n      up--;\n    }\n  }\n\n  // if the path is allowed to go above the root, restore leading ..s\n  if (allowAboveRoot) {\n    for (; up--; up) {\n      parts.unshift('..');\n    }\n  }\n\n  return parts;\n}\n\n// path.resolve([from ...], to)\n// posix version\nexports.resolve = function() {\n  var resolvedPath = '',\n      resolvedAbsolute = false;\n\n  for (var i = arguments.length - 1; i >= -1 && !resolvedAbsolute; i--) {\n    var path = (i >= 0) ? arguments[i] : process.cwd();\n\n    // Skip empty and invalid entries\n    if (typeof path !== 'string') {\n      throw new TypeError('Arguments to path.resolve must be strings');\n    } else if (!path) {\n      continue;\n    }\n\n    resolvedPath = path + '/' + resolvedPath;\n    resolvedAbsolute = path.charAt(0) === '/';\n  }\n\n  // At this point the path should be resolved to a full absolute path, but\n  // handle relative paths to be safe (might happen when process.cwd() fails)\n\n  // Normalize the path\n  resolvedPath = normalizeArray(filter(resolvedPath.split('/'), function(p) {\n    return !!p;\n  }), !resolvedAbsolute).join('/');\n\n  return ((resolvedAbsolute ? '/' : '') + resolvedPath) || '.';\n};\n\n// path.normalize(path)\n// posix version\nexports.normalize = function(path) {\n  var isAbsolute = exports.isAbsolute(path),\n      trailingSlash = substr(path, -1) === '/';\n\n  // Normalize the path\n  path = normalizeArray(filter(path.split('/'), function(p) {\n    return !!p;\n  }), !isAbsolute).join('/');\n\n  if (!path && !isAbsolute) {\n    path = '.';\n  }\n  if (path && trailingSlash) {\n    path += '/';\n  }\n\n  return (isAbsolute ? '/' : '') + path;\n};\n\n// posix version\nexports.isAbsolute = function(path) {\n  return path.charAt(0) === '/';\n};\n\n// posix version\nexports.join = function() {\n  var paths = Array.prototype.slice.call(arguments, 0);\n  return exports.normalize(filter(paths, function(p, index) {\n    if (typeof p !== 'string') {\n      throw new TypeError('Arguments to path.join must be strings');\n    }\n    return p;\n  }).join('/'));\n};\n\n\n// path.relative(from, to)\n// posix version\nexports.relative = function(from, to) {\n  from = exports.resolve(from).substr(1);\n  to = exports.resolve(to).substr(1);\n\n  function trim(arr) {\n    var start = 0;\n    for (; start < arr.length; start++) {\n      if (arr[start] !== '') break;\n    }\n\n    var end = arr.length - 1;\n    for (; end >= 0; end--) {\n      if (arr[end] !== '') break;\n    }\n\n    if (start > end) return [];\n    return arr.slice(start, end - start + 1);\n  }\n\n  var fromParts = trim(from.split('/'));\n  var toParts = trim(to.split('/'));\n\n  var length = Math.min(fromParts.length, toParts.length);\n  var samePartsLength = length;\n  for (var i = 0; i < length; i++) {\n    if (fromParts[i] !== toParts[i]) {\n      samePartsLength = i;\n      break;\n    }\n  }\n\n  var outputParts = [];\n  for (var i = samePartsLength; i < fromParts.length; i++) {\n    outputParts.push('..');\n  }\n\n  outputParts = outputParts.concat(toParts.slice(samePartsLength));\n\n  return outputParts.join('/');\n};\n\nexports.sep = '/';\nexports.delimiter = ':';\n\nexports.dirname = function (path) {\n  if (typeof path !== 'string') path = path + '';\n  if (path.length === 0) return '.';\n  var code = path.charCodeAt(0);\n  var hasRoot = code === 47 /*/*/;\n  var end = -1;\n  var matchedSlash = true;\n  for (var i = path.length - 1; i >= 1; --i) {\n    code = path.charCodeAt(i);\n    if (code === 47 /*/*/) {\n        if (!matchedSlash) {\n          end = i;\n          break;\n        }\n      } else {\n      // We saw the first non-path separator\n      matchedSlash = false;\n    }\n  }\n\n  if (end === -1) return hasRoot ? '/' : '.';\n  if (hasRoot && end === 1) {\n    // return '//';\n    // Backwards-compat fix:\n    return '/';\n  }\n  return path.slice(0, end);\n};\n\nfunction basename(path) {\n  if (typeof path !== 'string') path = path + '';\n\n  var start = 0;\n  var end = -1;\n  var matchedSlash = true;\n  var i;\n\n  for (i = path.length - 1; i >= 0; --i) {\n    if (path.charCodeAt(i) === 47 /*/*/) {\n        // If we reached a path separator that was not part of a set of path\n        // separators at the end of the string, stop now\n        if (!matchedSlash) {\n          start = i + 1;\n          break;\n        }\n      } else if (end === -1) {\n      // We saw the first non-path separator, mark this as the end of our\n      // path component\n      matchedSlash = false;\n      end = i + 1;\n    }\n  }\n\n  if (end === -1) return '';\n  return path.slice(start, end);\n}\n\n// Uses a mixed approach for backwards-compatibility, as ext behavior changed\n// in new Node.js versions, so only basename() above is backported here\nexports.basename = function (path, ext) {\n  var f = basename(path);\n  if (ext && f.substr(-1 * ext.length) === ext) {\n    f = f.substr(0, f.length - ext.length);\n  }\n  return f;\n};\n\nexports.extname = function (path) {\n  if (typeof path !== 'string') path = path + '';\n  var startDot = -1;\n  var startPart = 0;\n  var end = -1;\n  var matchedSlash = true;\n  // Track the state of characters (if any) we see before our first dot and\n  // after any path separator we find\n  var preDotState = 0;\n  for (var i = path.length - 1; i >= 0; --i) {\n    var code = path.charCodeAt(i);\n    if (code === 47 /*/*/) {\n        // If we reached a path separator that was not part of a set of path\n        // separators at the end of the string, stop now\n        if (!matchedSlash) {\n          startPart = i + 1;\n          break;\n        }\n        continue;\n      }\n    if (end === -1) {\n      // We saw the first non-path separator, mark this as the end of our\n      // extension\n      matchedSlash = false;\n      end = i + 1;\n    }\n    if (code === 46 /*.*/) {\n        // If this is our first dot, mark it as the start of our extension\n        if (startDot === -1)\n          startDot = i;\n        else if (preDotState !== 1)\n          preDotState = 1;\n    } else if (startDot !== -1) {\n      // We saw a non-dot and non-path separator before our dot, so we should\n      // have a good chance at having a non-empty extension\n      preDotState = -1;\n    }\n  }\n\n  if (startDot === -1 || end === -1 ||\n      // We saw a non-dot character immediately before the dot\n      preDotState === 0 ||\n      // The (right-most) trimmed path component is exactly '..'\n      preDotState === 1 && startDot === end - 1 && startDot === startPart + 1) {\n    return '';\n  }\n  return path.slice(startDot, end);\n};\n\nfunction filter (xs, f) {\n    if (xs.filter) return xs.filter(f);\n    var res = [];\n    for (var i = 0; i < xs.length; i++) {\n        if (f(xs[i], i, xs)) res.push(xs[i]);\n    }\n    return res;\n}\n\n// String.prototype.substr - negative index don't work in IE8\nvar substr = 'ab'.substr(-1) === 'b'\n    ? function (str, start, len) { return str.substr(start, len) }\n    : function (str, start, len) {\n        if (start < 0) start = str.length + start;\n        return str.substr(start, len);\n    }\n;\n\n/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../process/browser.js */ \"./node_modules/process/browser.js\")))\n\n//# sourceURL=webpack:///./node_modules/path-browserify/index.js?");

/***/ }),

/***/ "./node_modules/process/browser.js":
/*!*****************************************!*\
  !*** ./node_modules/process/browser.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("// shim for using process in browser\nvar process = module.exports = {};\n\n// cached from whatever global is present so that test runners that stub it\n// don't break things.  But we need to wrap it in a try catch in case it is\n// wrapped in strict mode code which doesn't define any globals.  It's inside a\n// function because try/catches deoptimize in certain engines.\n\nvar cachedSetTimeout;\nvar cachedClearTimeout;\n\nfunction defaultSetTimout() {\n    throw new Error('setTimeout has not been defined');\n}\nfunction defaultClearTimeout () {\n    throw new Error('clearTimeout has not been defined');\n}\n(function () {\n    try {\n        if (typeof setTimeout === 'function') {\n            cachedSetTimeout = setTimeout;\n        } else {\n            cachedSetTimeout = defaultSetTimout;\n        }\n    } catch (e) {\n        cachedSetTimeout = defaultSetTimout;\n    }\n    try {\n        if (typeof clearTimeout === 'function') {\n            cachedClearTimeout = clearTimeout;\n        } else {\n            cachedClearTimeout = defaultClearTimeout;\n        }\n    } catch (e) {\n        cachedClearTimeout = defaultClearTimeout;\n    }\n} ())\nfunction runTimeout(fun) {\n    if (cachedSetTimeout === setTimeout) {\n        //normal enviroments in sane situations\n        return setTimeout(fun, 0);\n    }\n    // if setTimeout wasn't available but was latter defined\n    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {\n        cachedSetTimeout = setTimeout;\n        return setTimeout(fun, 0);\n    }\n    try {\n        // when when somebody has screwed with setTimeout but no I.E. maddness\n        return cachedSetTimeout(fun, 0);\n    } catch(e){\n        try {\n            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally\n            return cachedSetTimeout.call(null, fun, 0);\n        } catch(e){\n            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error\n            return cachedSetTimeout.call(this, fun, 0);\n        }\n    }\n\n\n}\nfunction runClearTimeout(marker) {\n    if (cachedClearTimeout === clearTimeout) {\n        //normal enviroments in sane situations\n        return clearTimeout(marker);\n    }\n    // if clearTimeout wasn't available but was latter defined\n    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {\n        cachedClearTimeout = clearTimeout;\n        return clearTimeout(marker);\n    }\n    try {\n        // when when somebody has screwed with setTimeout but no I.E. maddness\n        return cachedClearTimeout(marker);\n    } catch (e){\n        try {\n            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally\n            return cachedClearTimeout.call(null, marker);\n        } catch (e){\n            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.\n            // Some versions of I.E. have different rules for clearTimeout vs setTimeout\n            return cachedClearTimeout.call(this, marker);\n        }\n    }\n\n\n\n}\nvar queue = [];\nvar draining = false;\nvar currentQueue;\nvar queueIndex = -1;\n\nfunction cleanUpNextTick() {\n    if (!draining || !currentQueue) {\n        return;\n    }\n    draining = false;\n    if (currentQueue.length) {\n        queue = currentQueue.concat(queue);\n    } else {\n        queueIndex = -1;\n    }\n    if (queue.length) {\n        drainQueue();\n    }\n}\n\nfunction drainQueue() {\n    if (draining) {\n        return;\n    }\n    var timeout = runTimeout(cleanUpNextTick);\n    draining = true;\n\n    var len = queue.length;\n    while(len) {\n        currentQueue = queue;\n        queue = [];\n        while (++queueIndex < len) {\n            if (currentQueue) {\n                currentQueue[queueIndex].run();\n            }\n        }\n        queueIndex = -1;\n        len = queue.length;\n    }\n    currentQueue = null;\n    draining = false;\n    runClearTimeout(timeout);\n}\n\nprocess.nextTick = function (fun) {\n    var args = new Array(arguments.length - 1);\n    if (arguments.length > 1) {\n        for (var i = 1; i < arguments.length; i++) {\n            args[i - 1] = arguments[i];\n        }\n    }\n    queue.push(new Item(fun, args));\n    if (queue.length === 1 && !draining) {\n        runTimeout(drainQueue);\n    }\n};\n\n// v8 likes predictible objects\nfunction Item(fun, array) {\n    this.fun = fun;\n    this.array = array;\n}\nItem.prototype.run = function () {\n    this.fun.apply(null, this.array);\n};\nprocess.title = 'browser';\nprocess.browser = true;\nprocess.env = {};\nprocess.argv = [];\nprocess.version = ''; // empty string to avoid regexp issues\nprocess.versions = {};\n\nfunction noop() {}\n\nprocess.on = noop;\nprocess.addListener = noop;\nprocess.once = noop;\nprocess.off = noop;\nprocess.removeListener = noop;\nprocess.removeAllListeners = noop;\nprocess.emit = noop;\nprocess.prependListener = noop;\nprocess.prependOnceListener = noop;\n\nprocess.listeners = function (name) { return [] }\n\nprocess.binding = function (name) {\n    throw new Error('process.binding is not supported');\n};\n\nprocess.cwd = function () { return '/' };\nprocess.chdir = function (dir) {\n    throw new Error('process.chdir is not supported');\n};\nprocess.umask = function() { return 0; };\n\n\n//# sourceURL=webpack:///./node_modules/process/browser.js?");

/***/ }),

/***/ "./src/assets/js/__module/add_ctl.es6":
/*!********************************************!*\
  !*** ./src/assets/js/__module/add_ctl.es6 ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("/**\n * 追加\n */\n\nmodule.exports = class AddCtl {\n\tconstructor(A){\n\t\tthis.A = A;\n\t\tconst B = this;\n\n\t\tthis.stat = 0;\n\t\tthis.div = document.querySelector('.add-dialog');\n\t\tthis.form = document.querySelector('.add-dialog .add-form');\n\n\t\tthis.group_box = document.querySelector('input[name=\"add-label-group\"]');\n\t\tthis.name_box = document.querySelector('input[name=\"add-label-name\"]');\n\t\tthis.path_box = document.querySelector('input[name=\"add-path\"]');\n\t\tthis.submit_button = document.querySelector('intpu[name=\"add-submit\"]');\n\t\tthis.group_list = document.querySelector('#add-group-list');\n\t\tthis.path_list = document.querySelector('#add-path-list');\n\n\t\tthis.open_button = document.querySelector('.buttons button.add');\n\t\tthis.close_button = document.querySelector('.add-dialog .close-button');\n\n\n\t\t// event listening\n\n\t\tthis.open_button.addEventListener('click', (evt) => {\n\t\t\tevt.preventDefault();\n\n\t\t\tif(this.stat == 0) this.activate();\n\t\t\telse this.deactivate();\n\t\t});\n\n\t\tthis.close_button.addEventListener('click', (evt) => {\n\t\t\tevt.preventDefault();\n\n\t\t\tif(this.stat == 1) this.deactivate();\n\t\t});\n\n\t\tthis.form.addEventListener('submit', (evt) => {\n\t\t\tevt.preventDefault();\n\n\t\t\tthis.A.add({\n\t\t\t\ttype: 'add',\n\t\t\t\tgroup: this.group_box.value,\n\t\t\t\tname: this.name_box.value,\n\t\t\t\tpath: this.path_box.value\n\t\t\t});\n\t\t});\n\t}\n\n\tasync initList(){\n\t\tthis.group_list.innerHTML = '';\n\t\tlet group_option_html = '';\n\t\tthis.A.list_ctl.groups.forEach((group) => {\n\t\t\tgroup_option_html += '<option value=\"' + group.label + '\">' + group.label + '</option>';\n\t\t});\n\t\tthis.group_list.innerHTML = group_option_html;\n\n\t\tthis.path_list.innerHTML = '';\n\t\tlet path_option_html = '';\n\t\tlet path_option_data = await this.A.api_ctl.suggested_path_list();\n\n\t\tpath_option_data.list.forEach((item) => {\n\t\t\tlet value = item;\n\t\t\tlet text = item.replace('path_option_data.sites_path' + '/','');\n\t\t\tpath_option_html += '<option value=\"' + value + '\">' + text + '</option>';\n\t\t});\n\t\tthis.path_list.innerHTML = path_option_html;\n\t}\n\n\tactivate(){\n\t\tif(this.stat != 0) return;\n\t\tthis.stat = 1;\n\n\t\tthis.initList();\n\t\tthis.group_box.value = '';\n\t\tthis.name_box.value = '';\n\t\tthis.path_box.value = '';\n\n\t\tthis.div.classList.remove('close');\n\t\tthis.div.classList.add('open');\n\t}\n\n\tdeactivate(){\n\t\tif(this.stat != 1) return;\n\t\tthis.stat = 0;\n\n\t\tthis.div.classList.add('close');\n\t\tthis.div.classList.remove('open');\n\t}\n}\n\n\n//# sourceURL=webpack:///./src/assets/js/__module/add_ctl.es6?");

/***/ }),

/***/ "./src/assets/js/__module/api_ctl.es6":
/*!********************************************!*\
  !*** ./src/assets/js/__module/api_ctl.es6 ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("/**\n * API Control\n */\n\n\nmodule.exports = class ApiCtl {\n\tconstructor(A){\n\t\tthis.A = A;\n\t\tconst B = this;\n\t\tconst path = __webpack_require__(/*! path */ \"./node_modules/path-browserify/index.js\");\n\n\t\tthis.api_url = path.join('api/app.php');\n\t\tconsole.log(this.api_url);\n\t}\n\n\tasync list(){\n\t\tconst data = { type: 'list' };\n\t\tconst res = await fetch(this.api_url, {\n\t\t\tmethod: 'POST',\n\t\t\theaders : {\n\t\t\t\t'Accept': 'application/json',\n\t\t\t\t'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'\n\t\t\t},\n\t\t\tbody: Object.keys(data).map((key)=>key+\"=\"+encodeURIComponent(data[key])).join(\"&\"),\n\t\t});\n\t\tconst ret = await res.json();\n\t\treturn ret;\n\t}\n\n\tasync select(data){\n\t\tconst res = await fetch(this.api_url, {\n\t\t\tmethod: 'POST',\n\t\t\theaders : {\n\t\t\t\t'Accept': 'application/json',\n\t\t\t\t'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'\n\t\t\t},\n\t\t\tbody: Object.keys(data).map((key)=>key+\"=\"+encodeURIComponent(data[key])).join(\"&\"),\n\t\t});\n\t\tconst ret = await res.json();\n\t\treturn ret;\n\t}\n\n\tasync add(data){\n\t\tconst res = await fetch(this.api_url, {\n\t\t\tmethod: 'POST',\n\t\t\theaders : {\n\t\t\t\t'Accept': 'application/json',\n\t\t\t\t'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'\n\t\t\t},\n\t\t\tbody: Object.keys(data).map((key)=>key+\"=\"+encodeURIComponent(data[key])).join(\"&\"),\n\t\t});\n\t\tconst ret = await res.json();\n\t\treturn ret;\n\t}\n\n\tasync remove(data){\n\n\t\tlet formdata = 'type=remove&';\n\t\tformdata += (data.map((path) => {\n\t\t\treturn 'remove_path[]=' + encodeURIComponent(path);\n\t\t})).join('&');\n\n\t\tconst res = await fetch(this.api_url, {\n\t\t\tmethod: 'POST',\n\t\t\theaders : {\n\t\t\t\t'Accept': 'application/json',\n\t\t\t\t'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'\n\t\t\t},\n\t\t\tbody: formdata,\n\t\t});\n\t\tconst ret = await res.json();\n\t\treturn ret;\n\t}\n\n\tasync suggested_path_list(){\n\t\tconst data = {\n\t\t\ttype: 'suggested_path_list'\n\t\t};\n\t\tconst res = await fetch(this.api_url, {\n\t\t\tmethod: 'POST',\n\t\t\theaders : {\n\t\t\t\t'Accept': 'application/json',\n\t\t\t\t'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'\n\t\t\t},\n\t\t\tbody: Object.keys(data).map((key)=>key+\"=\"+encodeURIComponent(data[key])).join(\"&\"),\n\t\t});\n\t\tconst ret = await res.json();\n\t\treturn ret;\n\t}\n}\n\n\n//# sourceURL=webpack:///./src/assets/js/__module/api_ctl.es6?");

/***/ }),

/***/ "./src/assets/js/__module/display_ctl.es6":
/*!************************************************!*\
  !*** ./src/assets/js/__module/display_ctl.es6 ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("/**\n * 表示部\n */\n\nmodule.exports = class DisplayCtl {\n\tconstructor(A){\n\t\tthis.A = A;\n\t\tconst B = this;\n\n\t\tthis.label_box = document.querySelector('.current-info .label');\n\t\tthis.path_box = document.querySelector('.current-info .path');\n\t}\n\n\tshow(data){\n\t\tlet label = '';\n\t\tlet path = '';\n\n\t\tdata.forEach((group) => {\n\t\t\tgroup.list.forEach((item) => {\n\t\t\t\tif(item.is_current == true){\n\t\t\t\t\tlabel = item.label;\n\t\t\t\t\tpath = item.path;\n\n\t\t\t\t\treturn false;\n\t\t\t\t}\n\t\t\t});\n\t\t});\n\n\t\tthis.label_box.innerHTML = label;\n\t\tthis.path_box.innerHTML = path;\n\t}\n}\n\n\n//# sourceURL=webpack:///./src/assets/js/__module/display_ctl.es6?");

/***/ }),

/***/ "./src/assets/js/__module/error_ctl.es6":
/*!**********************************************!*\
  !*** ./src/assets/js/__module/error_ctl.es6 ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("/**\n * エラーダイアログ\n */\n\nmodule.exports = class ErrorCtl {\n\tconstructor(A){\n\t\tthis.A = A;\n\t\tconst B = this;\n\n\t\tthis.stat = 0;\n\t\tthis.div = document.querySelector('.error-dialog');\n\t\tthis.code_box = document.querySelector('.error-dialog .code');\n\t\tthis.message_box = document.querySelector('.error-dialog .message');\n\t\tthis.close_button = document.querySelector('.error-dialog .close-button');\n\n\t\t// event listening\n\t\tthis.close_button.addEventListener('click', (evt) => {\n\t\t\tevt.preventDefault();\n\t\t\tthis.deactivate();\n\t\t});\n\t}\n\n\tactivate(code, message){\n\t\tif(this.stat != 0) return;\n\t\tthis.stat = 1;\n\n\t\tthis.code_box.innerHTML = code;\n\t\tthis.message_box.innerHTML = message;\n\n\t\tthis.div.classList.remove('close');\n\t\tthis.div.classList.add('open');\n\t}\n\n\tdeactivate(){\n\t\tif(this.stat != 1) return;\n\t\tthis.stat = 0;\n\n\t\tthis.div.classList.add('close');\n\t\tthis.div.classList.remove('open');\n\t}\n}\n\n\n//# sourceURL=webpack:///./src/assets/js/__module/error_ctl.es6?");

/***/ }),

/***/ "./src/assets/js/__module/form_ctl.es6":
/*!*********************************************!*\
  !*** ./src/assets/js/__module/form_ctl.es6 ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("/**\n * フォーム管理\n */\n\nmodule.exports = class FormCtl {\n\tconstructor(A){\n\t\tthis.A = A;\n\t\tconst B = this;\n\n\t\tthis.form = document.querySelector('.select-form');\n\t}\n}\n\n\n//# sourceURL=webpack:///./src/assets/js/__module/form_ctl.es6?");

/***/ }),

/***/ "./src/assets/js/__module/list_ctl.es6":
/*!*********************************************!*\
  !*** ./src/assets/js/__module/list_ctl.es6 ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("/**\n * list control\n */\n\n\nmodule.exports = class ListCtl {\n\tconstructor(A){\n\t\tthis.A = A;\n\t\tconst B = this;\n\n\t\tthis.form = document.querySelector('.select-form');\n\t\tthis.select = document.querySelector('#selector');\n\t\tthis.options = [];\n\t\tthis.groups = [];\n\n\t\tthis.form.addEventListener('submit', (evt) => {\n\t\t\tevt.preventDefault();\n\n\t\t\tlet send_data = {\n\t\t\t\ttype: 'select',\n\t\t\t\tpath: this.getValue()\n\t\t\t};\n\n\t\t\tA.select(send_data);\n\t\t});\n\t}\n\n\tsetList(data){\n\t\tconst B = this;\n\n\t\tthis.select.innerHTML = '';\n\t\tthis.options = [];\n\n\t\tlet selected = null;\n\n\t\tthis.groups = data.map((group_data) => {\n\t\t\tlet group = new ListCtlGroup(group_data, this.A, this);\n\t\t\tgroup.options.forEach((item) => {\n\t\t\t\tthis.select.appendChild(item.element);\n\t\t\t\titem.setIndex(this.options.length);\n\t\t\t\tthis.options.push(item);\n\n\t\t\t\tif(!!item.is_current) selected = item;\n\t\t\t});\n\n\t\t\treturn group;\n\t\t});\n\n\t\tif(!!selected){\n\t\t\tthis.activate(selected);\n\t\t}\n\t}\n\n\tactivate(obj){\n\t\tthis.options.forEach((item, index) => {\n\t\t\tif(obj == item){\n\t\t\t\titem.activate();\n\t\t\t\tthis.select.selectedIndex = item.index;\n\t\t\t}\n\t\t\telse{\n\t\t\t\titem.deactivate();\n\t\t\t}\n\t\t});\n\t}\n\n\tfocus(){\n\t\tthis.select.focus();\n\t}\n\n\tgetCurrentIndex(){\n\t\tlet ret = null;\n\t\tthis.options.forEach((item) => {\n\t\t\tif(item.is_current == true){\n\t\t\t\tret = item.index;\n\t\t\t\treturn false;\n\t\t\t}\n\t\t});\n\n\t\treturn ret;\n\t}\n\n\tgetValue(){\n\t\treturn this.select.options[this.select.selectedIndex].value;\n\t}\n}\n\nclass ListCtlGroup {\n\tconstructor(data, A, B){\n\t\tthis.A = A;\n\t\tthis.B = B;\n\t\tconst C = this;\n\n\t\tthis.label = data.group_label;\n\t\tthis.options = data.list.map((item, index) => {\n\t\t\treturn new ListCtlOption(item, index, this.A, this.B, this);\n\t\t});\n\t}\n}\n\nclass ListCtlOption {\n\tconstructor(data, group_index, A, B, C){\n\t\tthis.A = A;\n\t\tthis.B = B;\n\t\tthis.C = C;\n\t\tconst D = this;\n\n\t\tthis.stat = 0;\n\t\tthis.index = null;\n\t\tthis.group_index = group_index;\n\t\tthis.label = data.label;\n\t\tthis.path = data.path;\n\t\tthis.is_current = (data.is_current == true) ? true : false ;\n\t\tthis.createElement();\n\t}\n\n\tcreateElement(){\n\t\tthis.element = document.createElement('option');\n\t\tthis.element.value = this.path;\n\t\tthis.element.text = this.label;\n\n\t\tif(this.group_index == 0){\n\t\t\tthis.element.className = 'gf';\n\t\t}\n\t}\n\n\tsetIndex(n){\n\t\tthis.index = n;\n\t}\n\n\tactivate(){\n\t\tif(this.stat != 0) return;\n\t\tthis.stat = 1;\n\n\t\tthis.element.setAttribute('selected', 'selected');\n\t}\n\n\tdeactivate(){\n\t\tif(this.stat != 1) return;\n\t\tthis.stat = 0;\n\n\t\tthis.element.removeAttribute('selected');\n\t}\n}\n\n\n//# sourceURL=webpack:///./src/assets/js/__module/list_ctl.es6?");

/***/ }),

/***/ "./src/assets/js/__module/remove_complete_ctl.es6":
/*!********************************************************!*\
  !*** ./src/assets/js/__module/remove_complete_ctl.es6 ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("/**\n * 削除成功ダイアログ\n */\n\n\nmodule.exports = class RemoveCompleteCtl {\n\tconstructor(A){\n\t\tthis.A = A;\n\t\tconst B = this;\n\n\t\tthis.stat = 0;\n\t\tthis.div = document.querySelector('.remove-complete-dialog');\n\t\tthis.item_list = document.querySelector('.remove-complete-dialog .item-list');\n\t\tthis.close_button = document.querySelector('.remove-complete-dialog a.close-button');\n\t\tconsole.log(this.close_button);\n\n\t\tthis.close_button.addEventListener('click', (evt) => {\n\t\t\tevt.preventDefault();\n\t\t\tthis.deactivate();\n\t\t});\n\t}\n\n\tactivate(data){\n\t\tif(this.stat != 0) return;\n\t\tthis.stat = 1;\n\n\t\tthis.div.classList.remove('close');\n\t\tthis.div.classList.add('open');\n\t}\n\n\tdeactivate(){\n\t\tif(this.stat != 1) return;\n\t\tthis.stat = 0;\n\n\t\tthis.div.classList.remove('open');\n\t\tthis.div.classList.add('close');\n\t}\n}\n\n\n//# sourceURL=webpack:///./src/assets/js/__module/remove_complete_ctl.es6?");

/***/ }),

/***/ "./src/assets/js/__module/remove_ctl.es6":
/*!***********************************************!*\
  !*** ./src/assets/js/__module/remove_ctl.es6 ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("/**\n * 削除\n */\n\nmodule.exports = class RemoveCtl {\n\tconstructor(A){\n\t\tthis.A = A;\n\t\tconst B = this;\n\n\t\tthis.stat = 0;\n\t\tthis.div = document.querySelector('.remove-dialog');\n\t\tthis.form = document.querySelector('.remove-dialog .remove-form');\n\t\tthis.item_list = document.querySelector('.remove-dialog .item-list');\n\n\t\tthis.open_button = document.querySelector('.buttons button.remove');\n\t\tthis.close_button = document.querySelector('.remove-dialog .close-button');\n\n\n\t\t// event listening\n\n\t\tthis.open_button.addEventListener('click', (evt) => {\n\t\t\tevt.preventDefault();\n\n\t\t\tif(this.stat == 0) this.activate();\n\t\t\telse this.deactivate();\n\t\t});\n\n\t\tthis.close_button.addEventListener('click', (evt) => {\n\t\t\tevt.preventDefault();\n\n\t\t\tif(this.stat == 1) this.deactivate();\n\t\t});\n\n\t\tthis.form.addEventListener('submit', (evt) => {\n\t\t\tevt.preventDefault();\n\n\t\t\tlet checkbox = document.querySelectorAll('.remove-dialog input[type=\"checkbox\"]');\n\t\t\tlet data = [].filter.call(checkbox, item => item.checked == true).map(item => item.value);\n\n\t\t\tthis.A.remove(data);\n\t\t});\n\t}\n\n\tasync initList(){\n\t\tthis.item_list.innerHTML = '';\n\t\tlet html = '';\n\t\tconst list_data = await this.A.api_ctl.list();\n\t\tlist_data.forEach((group) => {\n\t\t\thtml += '<dt>' + group.group_label + '</dt>';\n\n\t\t\tgroup.list.forEach((item) => {\n\t\t\t\tlet label = (() => {\n\t\t\t\t\tif(item.label.indexOf('_') != -1){\n\t\t\t\t\t\treturn item.label.replace(/^([^_]+)_/,'');\n\t\t\t\t\t}\n\t\t\t\t\telse{\n\t\t\t\t\t\treturn item.label;\n\t\t\t\t\t}\n\t\t\t\t})();\n\t\t\t\thtml += '<dd><label>';\n\t\t\t\thtml += '<input type=\"checkbox\" name=\"remove_path[]\" value=\"' + item.path + '\">' + label;\n\t\t\t\thtml += '</label></dd>';\n\t\t\t});\n\t\t});\n\n\t\tthis.item_list.innerHTML = html;\n\t}\n\n\tactivate(){\n\t\tif(this.stat != 0) return;\n\t\tthis.stat = 1;\n\n\t\tthis.initList();\n\n\t\tthis.div.classList.remove('close');\n\t\tthis.div.classList.add('open');\n\t}\n\n\tdeactivate(){\n\t\tif(this.stat != 1) return;\n\t\tthis.stat = 0;\n\n\t\tthis.div.classList.add('close');\n\t\tthis.div.classList.remove('open');\n\t}\n}\n\n\n//# sourceURL=webpack:///./src/assets/js/__module/remove_ctl.es6?");

/***/ }),

/***/ "./src/assets/js/__module/success_ctl.es6":
/*!************************************************!*\
  !*** ./src/assets/js/__module/success_ctl.es6 ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("/**\n * 成功ダイアログ\n */\n\nmodule.exports = class SuccessCtl {\n\tconstructor(A){\n\t\tthis.A = A;\n\t\tconst B = this;\n\n\t\tthis.stat = 0;\n\t\tthis.div = document.querySelector('.success-dialog');\n\t\tthis.label_box = document.querySelector('.success-dialog .label');\n\t\tthis.path_box = document.querySelector('.success-dialog .path');\n\t\tthis.close_button = document.querySelector('.success-dialog a.close-button');\n\n\t\tthis.close_button.addEventListener('click', (evt) => {\n\t\t\tevt.preventDefault();\n\t\t\tthis.deactivate();\n\t\t});\n\t}\n\n\tactivate(data){\n\t\tif(this.stat != 0) return;\n\t\tthis.stat = 1;\n\n\t\tlet label = '';\n\t\tlet path = '';\n\n\t\tdata.forEach((group) => {\n\t\t\tgroup.list.forEach((item) => {\n\t\t\t\tif(item.is_current == true){\n\t\t\t\t\tlabel = item.label;\n\t\t\t\t\tpath = item.path;\n\n\t\t\t\t\treturn false;\n\t\t\t\t}\n\t\t\t});\n\t\t});\n\n\n\t\tthis.label_box.innerHTML = label;\n\t\tthis.path_box.innerHTML = path;\n\n\t\tthis.div.classList.remove('close');\n\t\tthis.div.classList.add('open');\n\n\t\tsetTimeout(() => {\n\t\t\tthis.deactivate();\n\t\t}, 3000);\n\t}\n\n\tdeactivate(){\n\t\tif(this.stat != 1) return;\n\t\tthis.stat = 0;\n\n\t\tthis.div.classList.remove('open');\n\t\tthis.div.classList.add('close');\n\t}\n}\n\n\n//# sourceURL=webpack:///./src/assets/js/__module/success_ctl.es6?");

/***/ }),

/***/ "./src/assets/js/app.es6":
/*!*******************************!*\
  !*** ./src/assets/js/app.es6 ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("\nclass App {\n\tconstructor(){\n\t\tconst A = this;\n\n\t\tconst ErrorCtl = __webpack_require__(/*! ./__module/error_ctl.es6 */ \"./src/assets/js/__module/error_ctl.es6\");\n\t\tthis.error_ctl = new ErrorCtl(this);\n\n\t\tconst SuccessCtl = __webpack_require__(/*! ./__module/success_ctl.es6 */ \"./src/assets/js/__module/success_ctl.es6\");\n\t\tthis.success_ctl = new SuccessCtl(this);\n\n\t\tconst ApiCtl = __webpack_require__(/*! ./__module/api_ctl.es6 */ \"./src/assets/js/__module/api_ctl.es6\");\n\t\tthis.api_ctl = new ApiCtl(this);\n\n\t\tconst ListCtl = __webpack_require__(/*! ./__module/list_ctl.es6 */ \"./src/assets/js/__module/list_ctl.es6\");\n\t\tthis.list_ctl = new ListCtl(this);\n\n\t\tconst DisplayCtl = __webpack_require__(/*! ./__module/display_ctl.es6 */ \"./src/assets/js/__module/display_ctl.es6\");\n\t\tthis.display_ctl = new DisplayCtl(this);\n\n\t\tconst FormCtl = __webpack_require__(/*! ./__module/form_ctl.es6 */ \"./src/assets/js/__module/form_ctl.es6\");\n\t\tthis.form_ctl = new FormCtl(this);\n\n\t\tconst AddCtl = __webpack_require__(/*! ./__module/add_ctl.es6 */ \"./src/assets/js/__module/add_ctl.es6\");\n\t\tthis.add_ctl = new AddCtl(this);\n\n\t\tconst RemoveCtl = __webpack_require__(/*! ./__module/remove_ctl.es6 */ \"./src/assets/js/__module/remove_ctl.es6\");\n\t\tthis.remove_ctl = new RemoveCtl(this);\n\n\t\tconst RemoveCompleteCtl = __webpack_require__(/*! ./__module/remove_complete_ctl.es6 */ \"./src/assets/js/__module/remove_complete_ctl.es6\");\n\t\tthis.remove_complete_ctl = new RemoveCompleteCtl(this);\n\t}\n\n\tasync init(){\n\t\tconst A = this;\n\n\t\tconst data = await this.api_ctl.list();\n\t\tconsole.log(data);\n\n\t\tif(data.error != undefined){\n\t\t\tthis.error_ctl.activate(data.error, data.stats.message);\n\t\t}\n\t\telse{\n\t\t\tthis.list_ctl.setList(data);\n\t\t\tthis.display_ctl.show(data);\n\n\t\t\tthis.list_ctl.focus();\n\t\t}\n\n\t}\n\n\tasync select(send_data){\n\t\tconst A = this;\n\n\t\tconst data = await this.api_ctl.select(send_data);\n\t\tconsole.log(data);\n\n\t\tif(data.error != undefined){\n\t\t\tthis.error_ctl.activate(data.error, data.stats.message);\n\t\t}\n\t\telse{\n\t\t\tthis.list_ctl.setList(data);\n\t\t\tthis.display_ctl.show(data);\n\n\t\t\tthis.list_ctl.focus();\n\n\t\t\tthis.success_ctl.activate(data);\n\t\t}\n\t}\n\n\tasync add(send_data){\n\t\tconst A = this;\n\n\t\tconst data = await this.api_ctl.select(send_data);\n\t\tconsole.log(data);\n\n\t\tif(data.error != undefined){\n\t\t\tthis.error_ctl.activate(data.error, data.stats.message);\n\t\t}\n\t\telse{\n\t\t\tthis.add_ctl.deactivate();\n\t\t\tthis.list_ctl.setList(data);\n\t\t\tthis.display_ctl.show(data);\n\n\t\t\tthis.list_ctl.focus();\n\n\t\t\tthis.success_ctl.activate(data);\n\t\t}\n\t}\n\n\tasync remove(send_data){\n\t\tconst A = this;\n\n\t\tconst data = await this.api_ctl.remove(send_data);\n\t\tconsole.log(data);\n\n\t\tif(data.error != undefined){\n\t\t\tthis.error_ctl.activate(data.error, data.stats.message);\n\t\t}\n\t\telse{\n\t\t\tthis.remove_ctl.deactivate();\n\t\t\tthis.list_ctl.setList(data);\n\t\t\tthis.display_ctl.show(data);\n\n\t\t\tthis.list_ctl.focus();\n\n\t\t\tthis.remove_complete_ctl.activate();\n\t\t}\n\t}\n}\n\n\ndocument.addEventListener('DOMContentLoaded', () => {\n\tconst app = new App();\n\tapp.init();\n});\n\n\n//# sourceURL=webpack:///./src/assets/js/app.es6?");

/***/ })

/******/ });