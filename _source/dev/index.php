<!DOCTYPE html>
<html class="main" lang="ja">

<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
	<meta charset="utf-8">
	<title>DocumentRoot Changer</title>
	<meta name="viewport" content="width=device-width">
	<link rel="stylesheet" type="text/css" href="/assets/css/style.css">
	<script src="/assets/js/vendor/jquery.min.js"></script>
	<script src="/assets/js/app.js?v=1.0.0"></script>
</head>

<body>
	<div class="wrap">
		<header class="main-header">
			<h1>DocumentRoot Changer</h1>
		</header>
		<main>
			<section class="current-info">
				<h2>現在のDocumentRootは</h2>
				<p class="label">本サイト</p>
				<p class="path">/Users/hiroki/Sites/jr-central.co.jp/htdocs</p>
			</section>
			<form class="select-form">
				<div class="selector">
					<select id="selector" name="q" size="10"></select>
				</div>
				<div class="buttons">
					<ul>
						<li>
							<input type="submit" name="submit" value="変更">
						</li>
						<li>
							<button class="add">追加</button>
						</li>
						<li>
							<button class="remove">削除</button>
						</li>
						<li>
							<button class="remove">編集</button>
						</li>
					</ul>
				</div>
			</form>
		</main>
		<section class="error-dialog">
			<div class="wrap">
				<h2>Error</h2>
				<p class="code">201</p>
				<p class="message">currentファイルの書き込みに失敗しました</p>
				<p class="help">よくわからない場合は<a href="https://tdwteamshare.slack.com/app_redirect?channel=@ichikawa.hiroki" target="_blank">@ichikawa.hiroki</a>まで</p>
				<ul class="buttons">
					<li><a class="close-button" href="#">閉じる</a>
					</li>
				</ul>
			</div>
		</section>
		<section class="success-dialog">
			<div class="wrap">
				<h2>Complete</h2>
				<h3>新しいDocumentRootは</h3>
				<p class="label">本サイト</p>
				<p class="path">/Users/hiroki/Sites/jr-central.co.jp/htdocs</p>
				<ul class="buttons">
					<li><a class="close-button" href="#">閉じる</a>
					</li>
				</ul>
			</div>
		</section>
		<section class="edit-dialog add-dialog">
			<form class="add-form wrap">
				<h2>追加</h2>
				<dl>
					<dt>
              <label for="add-label-group">名前</label>
            </dt>
					<dd class="label"><span>
                <input id="add-label-group" type="text" name="add-label-group" list="add-group-list" value="" placeholder="グループ">
                <datalist id="add-group-list"></datalist></span><span>_</span><span>
                <input id="add-label-name" type="text" name="add-label-name" value="" placeholder="名前"></span>
					</dd>
					<dt>
              <label for="add-path">パス</label>
            </dt>
					<dd class="path"><span>
                <input id="add-path" type="text" name="add-path" list="add-path-list" value="" placeholder="パス">
                <datalist id="add-path-list"></datalist></span>
					</dd>
				</dl>
				<ul class="buttons">
					<li>
						<input type="submit" name="add-submit" value="追加">
					</li>
					<li><a class="close-button" href="#">閉じる</a>
					</li>
				</ul>
			</form>
		</section>
		<section class="edit-dialog remove-dialog">
			<form class="remove-form wrap">
				<h2>削除</h2>
				<dl class="item-list"></dl>
				<ul class="buttons">
					<li>
						<input type="submit" name="remove-submit" value="削除">
					</li>
					<li><a class="close-button" href="#">閉じる</a>
					</li>
				</ul>
			</form>
		</section>
		<section class="remove-complete-dialog">
			<div class="wrap">
				<h2>完了</h2>
				<h3>削除完了しました</h3>
				<ul class="buttons">
					<li><a class="close-button" href="#">閉じる</a>
					</li>
				</ul>
			</div>
		</section>
		<footer class="site-footer"></footer>
	</div>
</body>

</html>