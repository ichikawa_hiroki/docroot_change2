/**
 * gulp task
 * image
 */

const config = require('../gulp.config.js')
	,util = require('../gulp.util.js')
;


const gulp = require('gulp')
	path = require('path')
	plumber = require('gulp-plumber')
	notify = require('gulp-notify')
	imagemin = require('gulp-imagemin')
	mozjpeg = require('imagemin-mozjpeg')
	pngquant = require('imagemin-pngquant')
	gifsicle = require('imagemin-gifsicle')
	svgo = require('imagemin-svgo')
;

/**
 * minify images
 * @return {object} gulp
 */
const image_minify = () => {

	let src_file = [
		path.join(config.path.src, '**/*.{jpg,jpeg,png,svg,gif}')
	];

	let dest_dir = config.path.dev;


	if(config.image.is_minify == true){
		return gulp
			.src(src_file, {
				cwd: './',
				base: config.path.src,
				since: gulp.lastRun(image_minify)
			})
			.pipe(plumber({
				errorHandler: notify.onError("Error: <%= error.message %>")
			}))
			.pipe(imagemin([
				pngquant({
					quality: config.image.png,
					speed: 1
				}),
				mozjpeg({
					quality: config.image.jpg,
					progressive: true
				}),
				gifsicle({
					interlaced: false,
					optimizationLevel: 3,
					colors: config.image.gif
				}),
				svgo()
			]))
			.pipe(gulp.dest(dest_dir, {
				cwd: './'
			}))
		;
	}
	else{
		return gulp
			.src(src_file, {
				cwd: './',
				since: gulp.lastRun(image_minify)
			})
			.pipe(plumber({
				errorHandler: notify.onError("Error: <%= error.message %>")
			}))
			.pipe(gulp.dest(dest_dir, {
				cwd: './'
			}))
		;
	}
}
module.exports.image_minify = image_minify;
gulp.task('image_minify', image_minify);


/**
 * watch html and others
 * @return {object} gulp
 */
const image_watch = () => {

	// let src_file = path.join(config.path.src, '**/*.{jpg,jpeg,png,svg,gif}');
	let src_file = config.path.src + '/**/*.{jpg,jpeg,png,svg,gif}';
	return gulp.watch(src_file, gulp.series('image_minify'));
}
module.exports.image_watch = image_watch;
gulp.task('image_watch', image_watch);
