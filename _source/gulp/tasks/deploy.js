/**
 * gulp task
 * deplay
 *
 * デプロイに関するtask
 */

const config = require('../gulp.config.js')
	,util = require('../gulp.util.js')
;

const gulp = require('gulp')
	path = require('path')
	plumber = require('gulp-plumber')
	notify = require('gulp-notify')
	replace = require('gulp-replace')
	iconv = require('gulp-iconv')
	uglify = require('gulp-uglify')
	cleanCss = require('gulp-clean-css')
;


/**
 * cssの圧縮
 * @param  {Function} callback コールバック
 * @return {Object}            gulp
 */
const css_minify = (callback) => {

	if(!!config.deploy.minify.css){
		console.log('CSS圧縮');
		let src_file = path.join(config.path.dev, '**/*.css');
		let dest_dir = config.path.dev;

		return gulp
			.src(src_file, {
				cwd: './',
				base: config.path.dev,
				since: gulp.lastRun(css_minify)
			})
			.pipe(plumber({
				errorHandler: notify.onError("Error: <%= error.message %>") //<-
			}))
			.pipe(cleanCss())
			.pipe(gulp.dest(dest_dir, {
				cwd: './'
			}))
		;
	}
	else callback();
}
module.exports.css_minify = css_minify;
gulp.task('css_minify', css_minify);


/**
 * jsの圧縮
 * @param  {Function} callback コールバック
 * @return {Object}            gulp
 */
const js_minify = (callback) => {

	if(!!config.deploy.minify.js){
		console.log('JS圧縮');
		let src_file = path.join(config.path.dev, '**/*.js');
		let dest_dir = config.path.dev;

		return gulp
			.src(src_file, {
				cwd: './',
				base: config.path.dev,
				since: gulp.lastRun(js_minify)
			})
			.pipe(plumber({
				errorHandler: notify.onError("Error: <%= error.message %>") //<-
			}))
			.pipe(uglify())
			.pipe(gulp.dest(dest_dir, {
				cwd: './'
			}))
		;
	}
	else callback();
}
module.exports.js_minify = js_minify;
gulp.task('js_minify', js_minify);



/**
 * 文字コード変換のためのテキスト置換
 * @param  {Function} callback コールバック
 * @return {Object}            gulp
 */
const text_replace = (callback) => {

	let src_file = [];
	if(config.deploy.encoding.html != 'UTF-8') src_file.push(path.join(config.path.dev, '**/*.html'));
	if(config.deploy.encoding.css != 'UTF-8') src_file.push(path.join(config.path.dev, '**/*.css'));
	if(config.deploy.encoding.js != 'UTF-8') src_file.push(path.join(config.path.dev, '**/*.js'));

	if(src_file.length > 0){
		console.log('テキスト置換');

		let dest_dir = config.path.dev;

		return gulp
			.src(src_file, {
				cwd: './',
				base: config.path.dev,
				since: gulp.lastRun(text_replace)
			})
			.pipe(plumber({
				errorHandler: notify.onError("Error: <%= error.message %>") //<-
			}))
			.pipe(replace('<meta charset="utf-8">','<meta charset="' + config.deploy.encoding + '">'))
			.pipe(replace('<meta charset="UTF-8">','<meta charset="' + config.deploy.encoding + '">'))
			.pipe(replace('@charset "utf-8";','@charset "' + config.deploy.encoding + '";'))
			.pipe(replace('@charset "UTF-8";','@charset "' + config.deploy.encoding + '";'))
			.pipe(replace('〜','～'))
			.pipe(gulp.dest(dest_dir, {
				cwd: './'
			}))
		;
	}
	else callback();
}
module.exports.text_replace = text_replace;
gulp.task('text_replace', text_replace);


/**
 * 文字エンコーディング変換
 * @param  {Function} callback コールバック
 * @return {Object}            gulp
 */
const text_encoding = (callback) => {

	let src_file = [];
	if(config.deploy.encoding.html != 'UTF-8') src_file.push(path.join(config.path.dev, '**/*.html'));
	if(config.deploy.encoding.css != 'UTF-8') src_file.push(path.join(config.path.dev, '**/*.css'));
	if(config.deploy.encoding.js != 'UTF-8') src_file.push(path.join(config.path.dev, '**/*.js'));


	if(src_file.length > 0){
		console.log('文字エンコーディング変換');

		let dest_dir = config.path.dev;

		return gulp
			.src(src_file, {
				cwd: './',
				base: config.path.dev,
				since: gulp.lastRun(text_encoding)
			})
			.pipe(plumber({
				errorHandler: notify.onError("Error: <%= error.message %>") //<-
			}))
			.pipe(iconv({
				decoding: 'utf8',
				encoding: config.deploy.encoding
			}))
			.pipe(gulp.dest(dest_dir, {
				cwd: './'
			}))
		;
	}
	else callback();
}
module.exports.text_encoding = text_encoding;
gulp.task('text_encoding', text_encoding);



/**
 * ファイルをコピー
 * @return {Object}            gulp
 */
const file_copy = () => {

	let src_file = [
		path.join(config.path.dev, '**/*')
	];

	if(!config.deploy.is_jsmap) src_file.push('!' + path.join(config.path.dev, '**/*.js.map'));
	if(!config.deploy.is_cssmap) src_file.push('!' + path.join(config.path.dev, '**/*.css.map'));
	if(!!config.deploy.is_sass) src_file.push(path.join(config.path.src, '**/*.scss'));

	config.path.ignore_prefix.forEach((item) => {
		src_file.push('!' + path.join(config.path.dev, '**' , item + '*'));
	});

	config.path.ignore_suffix.forEach((item) => {
		src_file.push('!' + path.join(config.path.dev, '**' , '*' + item + '.*'));
	});

	let dest_dir = config.path.deploy;

	return gulp
		.src(src_file, {
			cwd: './',
			base: config.path.dev,
			since: gulp.lastRun(file_copy)
		})
		.pipe(plumber({
			errorHandler: notify.onError("Error: <%= error.message %>") //<-
		}))
		.pipe(gulp.dest(dest_dir, {
			cwd: './'
		}))
	;
}
module.exports.file_copy = file_copy;
gulp.task('file_copy', file_copy);
