/**
 * gulp task
 * sass compile
 */

const config = require('../gulp.config.js')
	,util = require('../gulp.util.js')
;


const gulp = require('gulp')
	path = require('path')
	plumber = require('gulp-plumber')
	notify = require('gulp-notify')
	sass = require('gulp-sass')
	autoprefixer = require('gulp-autoprefixer')
;

/**
 * compile sass
 * @return {object} gulp
 */
const sass_compile = () => {

	let src_file = path.join(config.path.src, '**/*.scss');
	let dest_dir = config.path.dev;

	return gulp
		.src(src_file, {
			cwd: './',
			base: config.path.src,
			sourcemaps: true
		})
		.pipe(plumber({
			errorHandler: notify.onError("Error: <%= error.message %>") //<-
		}))
		.pipe(sass({
			onError: util.handleErrors,
			outputStyle: 'expanded',
			indentType: 'tab',
			indentWidth: 1
		}))
		.pipe(autoprefixer({
			cascade: false,
			grid: true
		}))
		.pipe(gulp.dest(dest_dir, {
			cwd: './',
			sourcemaps: '.'
		}))
	;
}
module.exports.sass_compile = sass_compile;
gulp.task('sass_compile', sass_compile);


/**
 * watch sass
 * @return {object} gulp
 */
const sass_watch = () => {

	// let src_file = path.join(config.path.src, '**/*.scss');
	let src_file = config.path.src + '/**/*.scss';
	return gulp.watch(src_file, gulp.series('sass_compile'));
}
module.exports.sass_watch = sass_watch;
gulp.task('sass_watch', sass_watch);
