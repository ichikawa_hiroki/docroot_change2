/**
 * gulp task
 * pug compile
 *
 * phpは.php.pugという拡張子でpugファイルを作ってください
 * .phpの拡張子でビルドされます
 */

const config = require('../gulp.config.js')
	,util = require('../gulp.util.js')
;


const gulp = require('gulp')
	path = require('path')
	plumber = require('gulp-plumber')
	notify = require('gulp-notify')
	pug = require('gulp-pug')
	rename = require('gulp-rename')
	beautify = require('gulp-html-prettify')
;

/**
 * compile pug
 * @return {object} gulp
 */
const pug_compile = () => {

	let src_file = [
		path.join(config.path.src, '**/*.pug'),
		'!' + path.join(config.path.src, '**/_*.pug'),
	];

	let dest_dir = config.path.dev;

	return gulp
		.src(src_file, {
			cwd: './',
			base: config.path.src
		})
		.pipe(plumber({
			errorHandler: notify.onError("Error: <%= error.message %>") //<-
		}))
		.pipe(pug({
			pretty: true
		}))
		.pipe(beautify({
			indent_char: '\t',
			indent_size: 1
 		}))
		.pipe(rename((path) => {
			if(path.basename.indexOf('.php') != -1){
				path.basename = path.basename.replace('.php','');
				path.extname = '.php';
			}
		}))
		.pipe(gulp.dest(dest_dir, {
			cwd: './'
		}))
	;
}
module.exports.pug_compile = pug_compile;
gulp.task('pug_compile', pug_compile);


/**
 * watch pug
 * @return {object} gulp
 */
const pug_watch = () => {

	// let src_file = path.join(config.path.src, '**/*.pug');
	let src_file = config.path.src + '/**/*.pug';
	return gulp.watch(src_file, gulp.series('pug_compile'));
}
module.exports.pug_watch = pug_watch;
gulp.task('pug_watch', pug_watch);
