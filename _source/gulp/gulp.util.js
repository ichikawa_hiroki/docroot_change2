/**
 * gulpファイル内で使う関数群
 *
 */



/**
 * Handler for errors. Shows the notification in the browser and the console.
 *
 * @param {Object|String} e
 * @returns {boolean}
 */
 const handleErrors = (e) => {
	var message = null;

	if (typeof e === 'object') message = e.message;
	if (typeof e === 'string') message = e;

	browserSync.notify(message);
	console.log(message);
	return true;
}
module.exports.handleErrors = handleErrors;
