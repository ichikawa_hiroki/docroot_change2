<?php

require_once('config.php');

// httpd管理
require_once('httpd.class.php');
$httpdctl = new TDW\dev\DocrootChange\Httpd\HttpdControl();

// ファイル管理
require_once('file.class.php');
$filectl = new TDW\dev\DocrootChange\File\FileControl(PATH_DATAFILE, PATH_CURRENTFILE);

// データ管理
require_once('data.class.php');
$datactl = new TDW\dev\DocrootChange\Data\DataControl($filectl);





header('Content-Type: application/json');

if(isset($_POST['type'])){

	// リストを返す
	if($_POST['type'] == 'list'){
		$json = $datactl->writeList();
		echo $json;

		// echo json_encode(array(
		// 	'error' => 101,
		// 	'stats' => array(
		// 		'message' => 'エラーのテスト'
		// 	)
		// ));

	}

	// ドキュメントルートを変更する
	else if($_POST['type'] == 'select'){
		if(isset($_POST['path'])){
			if(false !== $datactl->selectArticle($_POST['path'])){
				if($mes = ($httpdctl->graceful()) != ''){
					echo json_encode(array(
						'error' => 999,
						'stats' => array(
							'message' => 'current.txtは更新したが、サーバ再起動に失敗\n' . $mes
						)
					));
				}
				else{
					$json = $datactl->writeList();
					echo $json;
				}
			}
			else{
				echo json_encode(array(
					'error' => 202,
					'stats' => array(
						'message' => 'current.txtに書き込み失敗'
					)
				));
			}
		}
		else{
			echo json_encode(array(
				'error' => 201,
				'stats' => array(
					'message' => 'パスが不正'
				)
			));
		}
	}

	// 追加する
	else if($_POST['type'] == 'add'){
		if(!empty($_POST['group']) && !empty($_POST['name']) && !empty($_POST['path'])){

			$group = $_POST['group'];
			$name = $_POST['name'];
			$path = $_POST['path'];

			if(true === ($tmp = $datactl->add($group, $name, $path))){
				if($mes = ($httpdctl->graceful()) != ''){
					echo json_encode(array(
						'error' => 999,
						'stats' => array(
							'message' => '追加は成功したが、サーバ再起動に失敗\n' . $mes
						)
					));
				}
				else{
					$json = $datactl->writeList();
					echo $json;
				}
			}
			else{
				echo json_encode($tmp);
			}
		}
		else{
			echo json_encode(array(
				'error' => 301,
				'stats' => array(
					'message' => '空欄がある'
				)
			));
		}
	}

	// 削除
	else if($_POST['type'] == 'remove'){
		if(!empty($_POST['remove_path'])){

			if(true === ($tmp = $datactl->remove($_POST['remove_path']))){
				$json = $datactl->writeList();
				echo $json;
			}
			else{
				echo json_encode($tmp);
			}
		}
		else{
			echo json_encode(array(
				'error' => 401,
				'stats' => array(
					'message' => 'パスが指定されていない'
				)
			));
		}
	}

	// ドキュメントルート候補を返す
	else if($_POST['type'] == 'suggested_path_list'){
		$tmp = $datactl->getSuggestedPathList('json');
		echo $tmp;
	}
}
else{
	echo json_encode(array(
		'error' => 001,
		'stats' => array(
			'message' => 'リクエストが不正'
		)
	));
}


// $datactl->save();
// var_dump($datactl->getArray());
