<?php
/**
 * data管理
 */

namespace TDW\dev\DocrootChange\Data;

class DataControl {

	public $datas = null;
	public $current = null;

	public $filectl = null;

	public function __construct($filectl = null){
		$this->setFileCtl($filectl);

		$this->current = new Current();
		$this->current->setPath($this->filectl->loadCurrent());

		$this->datas = new Datas();
		$this->datas->initArticles($this->filectl->loadData());
		$this->datas->setCurrent($this->current->getPath());

	}

	public function setFileCtl($filectl){
		$this->filectl = $filectl;
	}

	public function setDatafilePath($path = null){
		if(
			null === $path ||
			!is_file($path)
		){
			return false;
		}
		else{
			$this->path_datafile = $path;
			return false;
		}
	}

	public function save(){
		return $this->filectl->saveData($this->datas->getArticlesAs());
	}

	public function getArray(){
		return $this->datas->getArticlesAs('array', true);
	}

	public function add($label_group = '', $label_name = '', $path = ''){
		if(false !== ($tmp = $this->datas->getArticleByPath($path))){
			return array(
				'error' => 311,
				'stats' => array(
					'message' => '指定のパスは「' . $tmp->getLabel() . '」で登録済みです'
				)
			);
		}
		else if(!is_dir($path)){
			return array(
				'error' => 312,
				'stats' => array(
					'message' => '指定のパスにディレクトリが存在しません'
				)
			);
		}
		else{
			$label = $label_group . '_' . $label_name;

			$this->datas->addArticle($label, $path, true);
			if(false !== $this->save()){
				if($this->selectArticle($path)){
					return true;
				}
				else{
					return array(
						'error' => 314,
						'status' => array(
							'message' => '追加は成功しましたが、変更は出来ませんでした'
						)
					);
				}
			}
			else{
				return array(
					'error' => 313,
					'stats' => array(
						'message' => 'データファイルへの書き込みに失敗'
					)
				);
			}
		}

	}

	public function remove($paths = array()){
		if(!is_array($paths)) $paths = array($paths);

		$ret = true;

		foreach($paths as $path){
			if(false !== ($article = $this->datas->getArticleByPath($path))){
				$this->datas->removeArticle($article);
			}
			else{
				if(!is_array($ret)){
					$error = array(
						'error' => 412,
						'stats' => array(
							'message' => '指定のパスはリストに存在しない'
						)
					);
					$error['stats']['message'] .= '\n' . $path;
					$ret = $error;
				}
				else{
					$ret['stats']['message'] .= '\n' . $path;
				}
			}
		}

		if(true !== $ret) return $ret;

		if(false !== $this->save()){
			return true;
		}
		else{
			return array(
				'error' => 413,
				'stats' => array(
					'message' => 'データファイルへの書き込みに失敗'
				)
			);
		}

	}

	public function writeList($type = 'json', $include_group = true){
		if($type == 'json'){
			return $this->datas->getArticlesAs('json', true);
		}
	}

	public function selectArticle($path = null){
		if(false !== ($article = $this->datas->getArticleByPath($path))){
			$this->current->setPath($article->getPath(), true);
			$this->datas->setCurrent($path);

			return $this->filectl->saveCurrent($path);
		}
		else{
			return false;
		}
	}

	public function getUserName(){
		preg_match("/\/Users\/([^\/]+)/", __FILE__, $match);
		if(isset($match[1])) return $match[1];
		else return null;
	}

	public function getSitesPath($type = 'string'){
		$sites_path = '/Users/' . $this->getUserName() . '/Sites';

		if($type == 'array'){
			return array(
				'sites_path' => $sites_path
			);
		}
		else if($type == 'json'){
			return json_encode(array(
				'sites_path' => $sites_path
			));

		}
		else{
			return $sites_path;
		}
	}

	public function getDocrootPathList($dir = null, $dig_count = 0){
		$ret = [];

		if($dir == null) $dir = $this->getSitesPath();
		if(!is_dir($dir)) return $ret;
		if($dig_count > DEPTH_DIG_DIRECTORY) return $ret;

		$pattern = "/htdocs$|public_html$|source\/.*\/dev$/";

		foreach(glob($dir . '/*') as $item){

			if(preg_match($pattern, $item)){
				$ret[] = $item;
			}
			else{
				$ret = array_merge($ret, $this->getDocrootPathList($item, $dig_count + 1));
			}
		}

		return $ret;
	}

	public function getSuggestedPathList($type = 'array'){
		$ret = [];


		foreach($this->getDocrootPathList() as $item){
			if(false === $this->datas->isPath($item)){
				$ret[] = $item;
			}
		}

		if($type == 'json'){
			return json_encode(array(
				'sites_path' => $this->getSitesPath(),
				'list' => $ret
			));
		}
		else{
			return $ret;
		}
	}
}



class Datas{

	public $articles = array();
	public $path_cache = array();

	public function __construct(){
		// $this->loadCSV($path);
	}

	public function initArticles($data){
		$this->articles = array();
		$this->path_cache = array();

		foreach($data as $row){
			$label = $row['label'];
			$path = $row['path'];
			$this->addArticle($label, $path);
		}

		$this->sortData();
	}

	public function sortData(){
		$s1 = array();
		$s2 = array();

		foreach($this->articles as $article){
			$s1[] = $article->getGroup();
			$s2[] = $article->getLabel();
		}

		array_multisort(
			$s1, SORT_ASC,
			$s2, SORT_ASC,
			$this->articles
		);
	}

	public function getArticleByLabel($label = ''){
		$ret = false;
		foreach($this->articles as $article){
			if($article->getLabel() == $label){
				$ret = $article;
				break;
			}
		}

		return $ret;
	}

	public function getArticleByPath($path = ''){
		$ret = false;
		foreach($this->articles as $article){
			if($article->getPath() == $path){
				$ret = $article;
				break;
			}
		}

		return $ret;
	}

	public function isPath($path){
		return (false !== array_search($path, $this->path_cache)) ? true : false ;
	}

	public function addArticle($label = '', $path = '', $ensort = false){
		if(false !== ($article = new Article($label, $path))){
			$this->articles[] = $article;
			$this->path_cache[] = $path;
			if(!!$ensort) $this->sortData();
			return true;
		}
		else return false;
	}

	public function removeArticle($article){
		$this->path_cache = array_diff($this->path_cache, array($article->getPath()));
		$tmp = array();
		foreach($this->articles as $a){
			if($a != $article){
				$tmp[] = $a;
			}
		}
		$this->articles = $tmp;
	}

	public function removeArticles($articles){
		foreach($articles as $article){
			$this->removeArticle($article);
		}
	}

	public function getArticlesAs($type = 'array', $include_group = false){

		if($type == 'json'){
			if(!!$include_group){
				$tmp = array();
				$ret = array();
				foreach($this->articles as $article){
					$group = $article->getGroup();
					if(!isset($tmp[$group])){
						$tmp[$group] = array();
					}
					$tmp[$group][] = $article->getAs('array');
				}

				foreach($tmp as $group_label => $list){
					$ret[] = array(
						'group_label' => $group_label,
						'list' => $list
					);
				}

				return json_encode($ret);
			}
			else{
				$ret = array();
				foreach($this->articles as $article){
					$ret[] = $article->getAs($type, $include_group);
				}
				return json_encode($ret);
			}
		}
		else{
			$ret = array();
			foreach($this->articles as $article){
				$ret[] = $article->getAs($type, $include_group);
			}
			return $ret;
		}
	}

	public function setCurrent($current_path){
		foreach($this->articles as $article){
			$article->setIsCurrent($current_path);
		}
	}
}

class Article {

	public $group = '';
	public $label = '';
	public $path = '';
	public $is_current = false;

	public function __construct($label = '', $path = ''){

		if(
			empty($label) ||
			empty($path)
		){
			return false;
		}

		$tmp = explode('_', $label);
		$this->group = $tmp[0];
		$this->label = $label;
		$this->path = $path;
	}

	public function getGroup(){
		return (string)$this->group;
	}

	public function getLabel(){
		return (string)$this->label;
	}

	public function getPath(){
		return (string)$this->path;
	}

	public function setIsCurrent($current_path){
		$this->is_current = ($this->path == $current_path);
	}

	public function getAs($type = 'array', $include_group = false){
		$ret = array(
			'label' => (string) $this->label,
			'path' => (string) $this->path,
			'is_current' => $this->is_current
		);
		if(true == $include_group){
			$ret['group'] = (string) $this->group;
		}

		if('array' == $type){
			return $ret;
		}
	}
}


class Current {
	public $path = null;

	public function __construct(){
	}

	public function getPath(){
		return $this->path;
	}

	public function setPath($str, $direct = false){
		if($direct == true){
			$this->path = $str;
		}
		else{
			$pattern = "/DocumentRoot \"([^\"]+)\"/";
			preg_match($pattern, $str, $match);
			$this->path = $match[1];
		}
	}

	public function is($path){
		return ($path == $this->path);
	}
}
