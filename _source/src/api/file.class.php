<?php
/**
 * ファイル管理
 */

namespace TDW\dev\DocrootChange\File;

class FileControl {
	public $data_file = null;
	public $current_file = null;

	public function __construct($data_file = null, $current_file = null){
		if(!$data_file || !$current_file || !is_file($data_file) || !is_file($current_file)){
			return false;
		}

		$this->data_file = $data_file;
		$this->current_file = $current_file;
	}

	public function loadData(){
		$file = new \SplFileObject($this->data_file, 'r');
		$file->setFlags(\SplFileObject::READ_CSV);

		$ret = array();

		foreach($file as $row){
			if(null === $row) continue;
			if(null === $row[0]) continue;

			$label = $row[0];
			$path = $row[1];

			$ret[] = array(
				'label' => $row[0],
				'path' => $row[1],
			);
		}

		return $ret;
	}

	public function loadCurrent(){
		$tmp = file_get_contents($this->current_file);
		return trim($tmp);
	}

	public function saveData($data){
		$ret = true;
		$file = new \SplFileObject($this->data_file, 'w');

		foreach($data as $row){
			if(false == $file->fputcsv($row)){
				$ret = false;
			}
		}

		return true;
	}

	public function saveCurrent($path){
		$str = sprintf("DocumentRoot \"%s\"\n", $path);
		if(false !== file_put_contents($this->current_file, $str)){
			return true;
		}
		else{
			return false;
		}
	}


}
