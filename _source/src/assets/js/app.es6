
class App {
	constructor(){
		const A = this;

		const ErrorCtl = require('./__module/error_ctl.es6');
		this.error_ctl = new ErrorCtl(this);

		const SuccessCtl = require('./__module/success_ctl.es6');
		this.success_ctl = new SuccessCtl(this);

		const ApiCtl = require('./__module/api_ctl.es6');
		this.api_ctl = new ApiCtl(this);

		const ListCtl = require('./__module/list_ctl.es6');
		this.list_ctl = new ListCtl(this);

		const DisplayCtl = require('./__module/display_ctl.es6');
		this.display_ctl = new DisplayCtl(this);

		const FormCtl = require('./__module/form_ctl.es6');
		this.form_ctl = new FormCtl(this);

		const AddCtl = require('./__module/add_ctl.es6');
		this.add_ctl = new AddCtl(this);

		const RemoveCtl = require('./__module/remove_ctl.es6');
		this.remove_ctl = new RemoveCtl(this);

		const RemoveCompleteCtl = require('./__module/remove_complete_ctl.es6');
		this.remove_complete_ctl = new RemoveCompleteCtl(this);
	}

	async init(){
		const A = this;

		const data = await this.api_ctl.list();
		console.log(data);

		if(data.error != undefined){
			this.error_ctl.activate(data.error, data.stats.message);
		}
		else{
			this.list_ctl.setList(data);
			this.display_ctl.show(data);

			this.list_ctl.focus();
		}

	}

	async select(send_data){
		const A = this;

		const data = await this.api_ctl.select(send_data);
		console.log(data);

		if(data.error != undefined){
			this.error_ctl.activate(data.error, data.stats.message);
		}
		else{
			this.list_ctl.setList(data);
			this.display_ctl.show(data);

			this.list_ctl.focus();

			this.success_ctl.activate(data);
		}
	}

	async add(send_data){
		const A = this;

		const data = await this.api_ctl.select(send_data);
		console.log(data);

		if(data.error != undefined){
			this.error_ctl.activate(data.error, data.stats.message);
		}
		else{
			this.add_ctl.deactivate();
			this.list_ctl.setList(data);
			this.display_ctl.show(data);

			this.list_ctl.focus();

			this.success_ctl.activate(data);
		}
	}

	async remove(send_data){
		const A = this;

		const data = await this.api_ctl.remove(send_data);
		console.log(data);

		if(data.error != undefined){
			this.error_ctl.activate(data.error, data.stats.message);
		}
		else{
			this.remove_ctl.deactivate();
			this.list_ctl.setList(data);
			this.display_ctl.show(data);

			this.list_ctl.focus();

			this.remove_complete_ctl.activate();
		}
	}
}


document.addEventListener('DOMContentLoaded', () => {
	const app = new App();
	app.init();
});
