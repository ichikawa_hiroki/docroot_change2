/**
 * 成功ダイアログ
 */

module.exports = class SuccessCtl {
	constructor(A){
		this.A = A;
		const B = this;

		this.stat = 0;
		this.div = document.querySelector('.success-dialog');
		this.label_box = document.querySelector('.success-dialog .label');
		this.path_box = document.querySelector('.success-dialog .path');
		this.close_button = document.querySelector('.success-dialog a.close-button');

		this.close_button.addEventListener('click', (evt) => {
			evt.preventDefault();
			this.deactivate();
		});
	}

	activate(data){
		if(this.stat != 0) return;
		this.stat = 1;

		let label = '';
		let path = '';

		data.forEach((group) => {
			group.list.forEach((item) => {
				if(item.is_current == true){
					label = item.label;
					path = item.path;

					return false;
				}
			});
		});


		this.label_box.innerHTML = label;
		this.path_box.innerHTML = path;

		this.div.classList.remove('close');
		this.div.classList.add('open');

		setTimeout(() => {
			this.deactivate();
		}, 3000);
	}

	deactivate(){
		if(this.stat != 1) return;
		this.stat = 0;

		this.div.classList.remove('open');
		this.div.classList.add('close');
	}
}
