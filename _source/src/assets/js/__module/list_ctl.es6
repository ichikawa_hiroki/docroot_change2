/**
 * list control
 */


module.exports = class ListCtl {
	constructor(A){
		this.A = A;
		const B = this;

		this.form = document.querySelector('.select-form');
		this.select = document.querySelector('#selector');
		this.options = [];
		this.groups = [];

		this.form.addEventListener('submit', (evt) => {
			evt.preventDefault();

			let send_data = {
				type: 'select',
				path: this.getValue()
			};

			A.select(send_data);
		});
	}

	setList(data){
		const B = this;

		this.select.innerHTML = '';
		this.options = [];

		let selected = null;

		this.groups = data.map((group_data) => {
			let group = new ListCtlGroup(group_data, this.A, this);
			group.options.forEach((item) => {
				this.select.appendChild(item.element);
				item.setIndex(this.options.length);
				this.options.push(item);

				if(!!item.is_current) selected = item;
			});

			return group;
		});

		if(!!selected){
			this.activate(selected);
		}
	}

	activate(obj){
		this.options.forEach((item, index) => {
			if(obj == item){
				item.activate();
				this.select.selectedIndex = item.index;
			}
			else{
				item.deactivate();
			}
		});
	}

	focus(){
		this.select.focus();
	}

	getCurrentIndex(){
		let ret = null;
		this.options.forEach((item) => {
			if(item.is_current == true){
				ret = item.index;
				return false;
			}
		});

		return ret;
	}

	getValue(){
		return this.select.options[this.select.selectedIndex].value;
	}
}

class ListCtlGroup {
	constructor(data, A, B){
		this.A = A;
		this.B = B;
		const C = this;

		this.label = data.group_label;
		this.options = data.list.map((item, index) => {
			return new ListCtlOption(item, index, this.A, this.B, this);
		});
	}
}

class ListCtlOption {
	constructor(data, group_index, A, B, C){
		this.A = A;
		this.B = B;
		this.C = C;
		const D = this;

		this.stat = 0;
		this.index = null;
		this.group_index = group_index;
		this.label = data.label;
		this.path = data.path;
		this.is_current = (data.is_current == true) ? true : false ;
		this.createElement();
	}

	createElement(){
		this.element = document.createElement('option');
		this.element.value = this.path;
		this.element.text = this.label;

		if(this.group_index == 0){
			this.element.className = 'gf';
		}
	}

	setIndex(n){
		this.index = n;
	}

	activate(){
		if(this.stat != 0) return;
		this.stat = 1;

		this.element.setAttribute('selected', 'selected');
	}

	deactivate(){
		if(this.stat != 1) return;
		this.stat = 0;

		this.element.removeAttribute('selected');
	}
}
