/**
 * 追加
 */

module.exports = class AddCtl {
	constructor(A){
		this.A = A;
		const B = this;

		this.stat = 0;
		this.div = document.querySelector('.add-dialog');
		this.form = document.querySelector('.add-dialog .add-form');

		this.group_box = document.querySelector('input[name="add-label-group"]');
		this.name_box = document.querySelector('input[name="add-label-name"]');
		this.path_box = document.querySelector('input[name="add-path"]');
		this.submit_button = document.querySelector('intpu[name="add-submit"]');
		this.group_list = document.querySelector('#add-group-list');
		this.path_list = document.querySelector('#add-path-list');

		this.open_button = document.querySelector('.buttons button.add');
		this.close_button = document.querySelector('.add-dialog .close-button');


		// event listening

		this.open_button.addEventListener('click', (evt) => {
			evt.preventDefault();

			if(this.stat == 0) this.activate();
			else this.deactivate();
		});

		this.close_button.addEventListener('click', (evt) => {
			evt.preventDefault();

			if(this.stat == 1) this.deactivate();
		});

		this.form.addEventListener('submit', (evt) => {
			evt.preventDefault();

			this.A.add({
				type: 'add',
				group: this.group_box.value,
				name: this.name_box.value,
				path: this.path_box.value
			});
		});
	}

	async initList(){
		this.group_list.innerHTML = '';
		let group_option_html = '';
		this.A.list_ctl.groups.forEach((group) => {
			group_option_html += '<option value="' + group.label + '">' + group.label + '</option>';
		});
		this.group_list.innerHTML = group_option_html;

		this.path_list.innerHTML = '';
		let path_option_html = '';
		let path_option_data = await this.A.api_ctl.suggested_path_list();

		path_option_data.list.forEach((item) => {
			let value = item;
			let text = item.replace('path_option_data.sites_path' + '/','');
			path_option_html += '<option value="' + value + '">' + text + '</option>';
		});
		this.path_list.innerHTML = path_option_html;
	}

	activate(){
		if(this.stat != 0) return;
		this.stat = 1;

		this.initList();
		this.group_box.value = '';
		this.name_box.value = '';
		this.path_box.value = '';

		this.div.classList.remove('close');
		this.div.classList.add('open');
	}

	deactivate(){
		if(this.stat != 1) return;
		this.stat = 0;

		this.div.classList.add('close');
		this.div.classList.remove('open');
	}
}
