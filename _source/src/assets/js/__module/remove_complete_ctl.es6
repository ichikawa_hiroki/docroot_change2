/**
 * 削除成功ダイアログ
 */


module.exports = class RemoveCompleteCtl {
	constructor(A){
		this.A = A;
		const B = this;

		this.stat = 0;
		this.div = document.querySelector('.remove-complete-dialog');
		this.item_list = document.querySelector('.remove-complete-dialog .item-list');
		this.close_button = document.querySelector('.remove-complete-dialog a.close-button');
		console.log(this.close_button);

		this.close_button.addEventListener('click', (evt) => {
			evt.preventDefault();
			this.deactivate();
		});
	}

	activate(data){
		if(this.stat != 0) return;
		this.stat = 1;

		this.div.classList.remove('close');
		this.div.classList.add('open');
	}

	deactivate(){
		if(this.stat != 1) return;
		this.stat = 0;

		this.div.classList.remove('open');
		this.div.classList.add('close');
	}
}
