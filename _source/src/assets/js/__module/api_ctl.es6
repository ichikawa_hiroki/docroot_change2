/**
 * API Control
 */


module.exports = class ApiCtl {
	constructor(A){
		this.A = A;
		const B = this;
		const path = require('path');

		this.api_url = path.join('api/app.php');
		console.log(this.api_url);
	}

	async list(){
		const data = { type: 'list' };
		const res = await fetch(this.api_url, {
			method: 'POST',
			headers : {
				'Accept': 'application/json',
				'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
			},
			body: Object.keys(data).map((key)=>key+"="+encodeURIComponent(data[key])).join("&"),
		});
		const ret = await res.json();
		return ret;
	}

	async select(data){
		const res = await fetch(this.api_url, {
			method: 'POST',
			headers : {
				'Accept': 'application/json',
				'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
			},
			body: Object.keys(data).map((key)=>key+"="+encodeURIComponent(data[key])).join("&"),
		});
		const ret = await res.json();
		return ret;
	}

	async add(data){
		const res = await fetch(this.api_url, {
			method: 'POST',
			headers : {
				'Accept': 'application/json',
				'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
			},
			body: Object.keys(data).map((key)=>key+"="+encodeURIComponent(data[key])).join("&"),
		});
		const ret = await res.json();
		return ret;
	}

	async remove(data){

		let formdata = 'type=remove&';
		formdata += (data.map((path) => {
			return 'remove_path[]=' + encodeURIComponent(path);
		})).join('&');

		const res = await fetch(this.api_url, {
			method: 'POST',
			headers : {
				'Accept': 'application/json',
				'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
			},
			body: formdata,
		});
		const ret = await res.json();
		return ret;
	}

	async suggested_path_list(){
		const data = {
			type: 'suggested_path_list'
		};
		const res = await fetch(this.api_url, {
			method: 'POST',
			headers : {
				'Accept': 'application/json',
				'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
			},
			body: Object.keys(data).map((key)=>key+"="+encodeURIComponent(data[key])).join("&"),
		});
		const ret = await res.json();
		return ret;
	}
}
