/**
 * 削除
 */

module.exports = class RemoveCtl {
	constructor(A){
		this.A = A;
		const B = this;

		this.stat = 0;
		this.div = document.querySelector('.remove-dialog');
		this.form = document.querySelector('.remove-dialog .remove-form');
		this.item_list = document.querySelector('.remove-dialog .item-list');

		this.open_button = document.querySelector('.buttons button.remove');
		this.close_button = document.querySelector('.remove-dialog .close-button');


		// event listening

		this.open_button.addEventListener('click', (evt) => {
			evt.preventDefault();

			if(this.stat == 0) this.activate();
			else this.deactivate();
		});

		this.close_button.addEventListener('click', (evt) => {
			evt.preventDefault();

			if(this.stat == 1) this.deactivate();
		});

		this.form.addEventListener('submit', (evt) => {
			evt.preventDefault();

			let checkbox = document.querySelectorAll('.remove-dialog input[type="checkbox"]');
			let data = [].filter.call(checkbox, item => item.checked == true).map(item => item.value);

			this.A.remove(data);
		});
	}

	async initList(){
		this.item_list.innerHTML = '';
		let html = '';
		const list_data = await this.A.api_ctl.list();
		list_data.forEach((group) => {
			html += '<dt>' + group.group_label + '</dt>';

			group.list.forEach((item) => {
				let label = (() => {
					if(item.label.indexOf('_') != -1){
						return item.label.replace(/^([^_]+)_/,'');
					}
					else{
						return item.label;
					}
				})();
				html += '<dd><label>';
				html += '<input type="checkbox" name="remove_path[]" value="' + item.path + '">' + label;
				html += '</label></dd>';
			});
		});

		this.item_list.innerHTML = html;
	}

	activate(){
		if(this.stat != 0) return;
		this.stat = 1;

		this.initList();

		this.div.classList.remove('close');
		this.div.classList.add('open');
	}

	deactivate(){
		if(this.stat != 1) return;
		this.stat = 0;

		this.div.classList.add('close');
		this.div.classList.remove('open');
	}
}
