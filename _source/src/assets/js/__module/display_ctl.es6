/**
 * 表示部
 */

module.exports = class DisplayCtl {
	constructor(A){
		this.A = A;
		const B = this;

		this.label_box = document.querySelector('.current-info .label');
		this.path_box = document.querySelector('.current-info .path');
	}

	show(data){
		let label = '';
		let path = '';

		data.forEach((group) => {
			group.list.forEach((item) => {
				if(item.is_current == true){
					label = item.label;
					path = item.path;

					return false;
				}
			});
		});

		this.label_box.innerHTML = label;
		this.path_box.innerHTML = path;
	}
}
