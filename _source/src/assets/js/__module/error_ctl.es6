/**
 * エラーダイアログ
 */

module.exports = class ErrorCtl {
	constructor(A){
		this.A = A;
		const B = this;

		this.stat = 0;
		this.div = document.querySelector('.error-dialog');
		this.code_box = document.querySelector('.error-dialog .code');
		this.message_box = document.querySelector('.error-dialog .message');
		this.close_button = document.querySelector('.error-dialog .close-button');

		// event listening
		this.close_button.addEventListener('click', (evt) => {
			evt.preventDefault();
			this.deactivate();
		});
	}

	activate(code, message){
		if(this.stat != 0) return;
		this.stat = 1;

		this.code_box.innerHTML = code;
		this.message_box.innerHTML = message;

		this.div.classList.remove('close');
		this.div.classList.add('open');
	}

	deactivate(){
		if(this.stat != 1) return;
		this.stat = 0;

		this.div.classList.add('close');
		this.div.classList.remove('open');
	}
}
