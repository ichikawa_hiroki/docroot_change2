/**
 * webpack config
 */


module.exports = {
	mode: 'development',

	entry: './src/assets/js/app.es6',
	output: {
		filename: 'assets/js/app.js'
	}
};
